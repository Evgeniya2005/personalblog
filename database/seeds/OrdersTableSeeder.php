<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name'=>'Ivan Ivanov',
                'email'=>'ivan@gmaail.com',
                'phone'=>932233444,
                'Feedback'=>'Все супер!',
            ],
            [
                'customer_name'=>'Tom Johns',
                'email'=>'tom@gmaail.com',
                'phone'=>932233555,
                'Feedback'=>'Все OK!',
            ],
            [
                'customer_name'=>'Tim Bim',
                'email'=>'tim@gmaail.com',
                'phone'=>932233777,
                'Feedback'=>'The best!',
            ]
        ]);
    }
}
