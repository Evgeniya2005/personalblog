<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('products')->insert([
           [
               'title'=>'Apple Watch Series 5',
               'slug'=>'products/watch',
               'price'=> 13999,
               'description'=>'Таких часов вы ещё не видели.Их дисплей никогда не спит.
                    На новом, всегда включённом дисплее Retina циферблат отображается постоянно — и время видно всё время.',
           ],
           [
               'title'=>'Серия: iPhone Xs',
               'slug'=>'products/phone',
               'price'=> 19999,
               'description'=>'Super Retina. Супер размеры
                Уникальные OLED‑дисплеи iPhone XS поддерживают HDR,
                обеспечивают непревзойдённую точность цветопередачи и глубокие чёрные цвета.
                При этом дисплей iPhone XS Max стал самым большим дисплеем в истории iPhone',
           ],
           [
               'title'=>'Apple MacBook Air 13',
               'slug'=>'products/laptop',
               'price'=> 30999,
               'description'=>'Это самый любимый Mac, в который можно заново влюбиться.
               Новый MacBook Air — ещё более тонкий и лёгкий, оснащён дисплеем Retina, Touch ID,
               клавиатурой нового поколения, трекпадом Force Touch.
               И представлен в трёх цветах — серебристом, золотом и «серый космос».',
           ]
       ]);
    }
}
