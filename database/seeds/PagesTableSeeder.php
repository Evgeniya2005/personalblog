<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title'=>'Dogs',
                'slug'=>'pages/dogs',
                'intro'=>'Соба́ка — домашнее животное',
                'content'=>'Соба́ка — домашнее животное, одно из наиболее популярных животных-компаньонов.
                Первоначально домашняя собака была выделена в отдельный биологический вид Линнеем в 1758 году,
                в 1993 году реклассифицирована Смитсоновским институтом и Американской ассоциацией териологов в подвид волка.',
            ],
            [
                'title'=>'Cats',
                'slug'=>'pages/cats',
                'intro'=>'Ко́шка, или дома́шняя ко́шка, — домашнее животное',
                'content'=>'Ко́шка, или дома́шняя ко́шка, — домашнее животное, одно из наиболее популярных «животных-компаньонов».
                С точки зрения научной систематики, домашняя кошка — млекопитающее семейства кошачьих отряда хищных.
                Ранее домашнюю кошку нередко рассматривали как отдельный биологический вид.',
            ],
            [
                'title'=>'Parrot',
                'slug'=>'pages/parrot',
                'intro'=>'птица',
                'content'=>'Волни́стый попуга́йчик — птица семейства попугаевых. Единственный вид в роде волнистые попугаи.
                Особи отличаются шумом и болтливостью, довольно легко запоминают слова и выражения,
                которые повторяют много раз при «общении» с человеком и даже с другими домашними птицами.',
            ]
        ]);
    }
}
